package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Week6.day1.LearnExcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}
@Parameters({"url","username","password"})
	@BeforeMethod()
	public void login(String url,String username,String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
		
	}
	@AfterMethod
	public void Merge() {
	}
	
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	@DataProvider(name = "fetchdata")
	public Object[][] fetchData() throws IOException
	{	
	return LearnExcel.getExcelData(excelFileName);
	}
	
	
	           
	
	
	
	
	
}
