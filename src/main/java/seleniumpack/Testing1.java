package seleniumpack;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Testing1 {
static WebDriver driver;
public static void screenshot() throws IOException
{
File Sshot=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
FileUtils.copyFile(Sshot, new File(System.getProperty("user.dir")+File.separator+"Screenshots"+File.separator+"png_images"+File.separator+System.currentTimeMillis()+".png"));
}
@BeforeMethod
public void browserinitiate()
{

String chromepath = System.getProperty("user.dir")+"\\chromedriver.exe";
System.setProperty("webdriver.chrome.driver", chromepath);
driver = new ChromeDriver();
driver.manage().window().maximize();
}

@AfterMethod
public void browserclose()
{
driver.close();
driver.quit();
}

@Test(dataProvider="soo",dataProviderClass=DemoExcel.class)
public void demotest1(String firstname, String lastname, String sex, String yoe, String date, String prof, String autotool, String cont, String selcom ) throws IOException {
screenshot();
	driver.get("http://toolsqa.com/automation-practice-form/");
WebElement FN = driver.findElement(By.name("firstname"));
FN.sendKeys(firstname);
driver.findElement(By.name("lastname")).sendKeys(lastname);
screenshot();
WebElement male = driver.findElement(By.xpath("//*[@value='Male']"));
WebElement female = driver.findElement(By.xpath("//*[@value='Female']"));
if(sex.equalsIgnoreCase("Male")) {
male.click();
} 
else if(sex.equalsIgnoreCase("Female")) {
female.click();
}

WebElement yoe1 = driver.findElement(By.id("exp-0"));
WebElement yoe2 = driver.findElement(By.id("exp-1"));
WebElement yoe3 = driver.findElement(By.id("exp-2"));
WebElement yoe4 = driver.findElement(By.id("exp-3"));
WebElement yoe5 = driver.findElement(By.id("exp-4"));
WebElement yoe6 = driver.findElement(By.id("exp-5"));
WebElement yoe7 = driver.findElement(By.id("exp-6"));
screenshot();
switch(yoe)
{
case "1": yoe1.click();
break;
case "2": yoe2.click();
break;
case "3": yoe3.click();
break;
case "4": yoe4.click();
break;
case "5": yoe5.click();
break;
case "6": yoe6.click();
break;
case "7": yoe7.click();
break;
default: yoe1.click();
break;

}

driver.findElement(By.id("datepicker")).sendKeys(date);
if(prof.equalsIgnoreCase("Manual Tester")) {
driver.findElement(By.id("profession-0")).click();
}
else if (prof.equalsIgnoreCase("Automation Tester")) {
driver.findElement(By.id("profession-1")).click();
screenshot();
}
switch(autotool)
{
case "QTP": driver.findElement(By.id("tool-0")).click();
break;
}
}
}