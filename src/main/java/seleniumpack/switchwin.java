package seleniumpack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class switchwin {
	static WebDriver driver;
	@BeforeMethod
	public void browserinitiate()
	{

	String chromepath = System.getProperty("user.dir")+"\\chromedriver.exe";
	System.setProperty("webdriver.chrome.driver", chromepath);
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	}
	@Test
	public void witch() throws InterruptedException
	{
		driver.get("http://toolsqa.com/automation-practice-switch-windows/");
		String parentwindow = driver.getWindowHandle();
		WebElement newbro = driver.findElement(By.xpath("//*[@id=\"button1\"]"));
		newbro.click();
		Thread.sleep(5000);
		
	for(String child : driver.getWindowHandles())
			{
		driver.switchTo().window(child);
			}
		String stor = driver.findElement(By.id("site-title")).getText();
		System.out.println(stor);
		driver.close();
		driver.switchTo().window(parentwindow);
		
		WebElement newmsg = driver.findElement(By.xpath("//*[@id=\"content\"]/p[3]/button"));
		newmsg.click();
		Thread.sleep(3000);
			
			for(String Child1 : driver.getWindowHandles())
			{
				driver.switchTo().window(Child1);
			}
			String str = driver.getTitle();
					System.out.println(str);
					driver.close();
	driver.switchTo().window(parentwindow);
	WebElement brotab = driver.findElement(By.xpath("//*[@id=\"content\"]/p[4]/button"));
	brotab.click();
	Thread.sleep(3000);
		
		for(String Child2 : driver.getWindowHandles())
		{
			driver.switchTo().window(Child2);
		}
		String tab = driver.findElement(By.xpath("//*[@id=\"slide-16-layer-5\"]/a")).getText();
				System.out.println(tab);
				driver.close();
				driver.switchTo().window(parentwindow);
}
}
	