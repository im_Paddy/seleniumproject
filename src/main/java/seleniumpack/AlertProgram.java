package seleniumpack;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver;
		System.setProperty("Webdriver.chrome.driver","C:\\Users\\padmanabhan.srinivas\\Desktop\\New folder(2)\\Selenium\\chromedriver.exe");
driver = new ChromeDriver();
driver.get("http://toolsqa.com/handling-alerts-using-selenium-webdriver/");
driver.manage().window().maximize();
WebElement simple = driver.findElement(By.xpath("//*[@id=\"content\"]/p[4]/button"));
System.out.println("clicking alert");
simple.click();
System.out.println("clicked");
driver.switchTo().alert().accept();
System.out.println("acepted alert");
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("window.scrollBy(0,250)", "");
WebElement Confirm = driver.findElement(By.xpath("//*[text()='Confirm Pop up']"));
Confirm.click();
System.out.println("clicked");
driver.switchTo().alert().dismiss();
System.out.println("dissmed alert");
WebElement prompt = driver.findElement(By.xpath("//*[text()='Prompt Pop up']"));
prompt.click();
System.out.println("clicked");
driver.switchTo().alert().sendKeys("yes");
driver.switchTo().alert().accept();
System.out.println("Yes clicked");
WebElement drop = driver.findElement(By.id("continents"));
	}
}
