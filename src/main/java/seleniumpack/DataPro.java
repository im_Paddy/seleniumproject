package seleniumpack;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.testng.annotations.DataProvider;


public class DataPro {

	@DataProvider(name = "excelll")
	public static  String[][] data() throws  IOException
	{
		Workbook workbook = Workbook.getWorkbook(new File("C:\\Users\\padmanabhan.srinivas\\Documents\\demo.xls"));
		Sheet sheet = workbook.getSheet(2);
		int row = sheet.getRows();
		int col=sheet.getColumns();
		
		String[][] excel = new String[row-1][col];
		
		
		for(int i=1;i<row;i++) {
			for(int j=0;j<col;j++) {
				Cell cell=sheet.getCell(j,i);
				excel[i-1][j]= cell.getContents();
			}
		}
		
		return excel;
	}
}
