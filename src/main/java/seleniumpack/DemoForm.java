package seleniumpack;

import java.awt.Desktop.Action;
import java.io.File;
import java.io.IOException;

import javax.swing.Spring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DemoForm {
	
	static String first;
	static String last;
	static String sex;
	static String year;
	static String date;
	static String prof;
	static String tool;
	static String conti;
	static String comm;

	public static void main(String[] args) throws BiffException, IOException {
		
		DemoForm exe = new DemoForm();
		exe.fetch();
		exe.browser();
		
		
	}
	
	public static void fetch() throws BiffException, IOException
	{
	Workbook wb = Workbook.getWorkbook(new File("C:\\Users\\padmanabhan.srinivas\\Documents\\demo.xls")); 
	Sheet sh = wb.getSheet(1);
     first = sh.getCell(0,1).getContents();
    last = sh.getCell(1,1).getContents();
    sex = sh.getCell(2,1).getContents();
     year = sh.getCell(3,1).getContents();
    date = sh.getCell(4,1).getContents();
     prof = sh.getCell(5,1).getContents();
     tool = sh.getCell(6,1).getContents();
     conti = sh.getCell(7,1).getContents();
     comm = sh.getCell(8,1).getContents();
     }
	public static void browser ()
	{
		WebDriver driver;
		System.setProperty("Webdriver.chrome.driver","C:\\Users\\padmanabhan.srinivas\\Desktop\\New folder (2)\\Selenium\\chromedriver.exe" );
		driver = new ChromeDriver();
		driver.get("http://toolsqa.com/automation-practice-form/");
		driver.manage().window().maximize();
		driver.findElement(By.name("firstname")).sendKeys(DemoForm.first);
		driver.findElement(By.name("lastname")).sendKeys(DemoForm.last);
		driver.findElement(By.xpath("//*[@id=\"sex-0\"]"));
		driver.findElement(By.xpath("//*[@id=\"sex-1\"]"));
		if(DemoForm.sex.equalsIgnoreCase("Male"))
		{
			driver.findElement(By.xpath("//*[@id=\"sex-0\"]")).click();
		}
			else if (DemoForm.sex.equalsIgnoreCase("Female"))
			{
				driver.findElement(By.xpath("//*[@id=\"sex-1\"]")).click();
			}
		
		WebElement ex1= driver.findElement(By.id("exp-0"));
		WebElement ex2= driver.findElement(By.id("exp-1"));
		WebElement ex3= driver.findElement(By.id("exp-2"));
		WebElement ex4= driver.findElement(By.id("exp-3"));
		WebElement ex5= driver.findElement(By.id("exp-4"));
		WebElement ex6= driver.findElement(By.id("exp-5"));
		WebElement ex7= driver.findElement(By.id("exp-6"));
		switch (DemoForm.year)
		{
		case "1" : ex1.click();
		break;
		case "2" : ex2.click();
		break;
		case "3" : ex3.click();
		break;
		case "4" : ex4.click();
		break;
		case "5" : ex5.click();
		break;
		case "6" : ex6.click();
		break;
		case "7" : ex7.click();
		break;		
		}
		if(DemoForm.prof.equalsIgnoreCase("Manual tester"))
		{
			driver.findElement(By.id("profession-0")).click();
		}
		else if(DemoForm.prof.equalsIgnoreCase("Automation Tester"))
		{
			driver.findElement(By.id("profession-1")).click();
		}
		new Select(driver.findElement(By.id("continents"))).selectByVisibleText(DemoForm.conti);
		
		if(DemoForm.tool.equalsIgnoreCase("QTP"))
		{
			driver.findElement(By.id("tool-0")).click();
		}
		else if(DemoForm.tool.equalsIgnoreCase("selenium ide"))
		{
			driver.findElement(By.id("tool-1")).click();
		}
		else if (DemoForm.tool.equalsIgnoreCase("selenium webdriver"))
		{
			driver.findElement(By.id("tool-2")).click();
		}
		
		driver.findElement(By.id("datepicker")).sendKeys("20/01/2018");
		Actions h = new Actions(driver);

		
		WebElement com1 = driver.findElement(By.xpath("//*[@id=\"selenium_commands\"]/option[1]"));
		WebElement com2 = driver.findElement(By.xpath("//*[@id=\"selenium_commands\"]/option[2]"));
		WebElement com3 = driver.findElement(By.xpath("//*[@id=\"selenium_commands\"]/option[3]"));
		WebElement com4 = driver.findElement(By.xpath("//*[@id=\"selenium_commands\"]/option[4]"));
		WebElement com5 = driver.findElement(By.xpath("//*[@id=\"selenium_commands\"]/option[5]"));
		
		switch (DemoForm.comm)
		{
		case "1" : com1.click();
		break;
		case "2" : com2.click();
		break;
		case "3" : com3.click();
		break;
		case "4" : com4.click();
		break;
		case "5" : com5.click();
		break;
		}
		
		driver.findElement(By.id("submit")).click();
		System.out.println("Form submitted ");
		}
	
}

