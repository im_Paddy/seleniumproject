package seleniumpack;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapHash {

	public static void main(String[] args) {

		String str = "Padmanabhan";
		char[] c = str.toCharArray();
		Map<Character, Integer> mp = new HashMap<Character, Integer>();
		for (char cr : c) 
		{
			if(mp.containsKey(cr))
			{
				Integer ct = mp.get(cr)+1;

				mp.put(cr, ct);
			}
			else
			{
				mp.put(cr, 1);
			}
		}
		int max =0;
		char key = 0;
		for (Entry<Character, Integer> chr : mp.entrySet()) {

			if (max < chr.getValue()) {
				max = chr.getValue();
				 key = chr.getKey();
			}

		}
		System.out.println("Number of occurances for each character: " +key);

	}
}
