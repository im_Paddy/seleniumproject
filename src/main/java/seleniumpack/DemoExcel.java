package seleniumpack;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DemoExcel {
	static WebDriver driver;
int row;
int col;
	@DataProvider(name = "soo")
	public String[][] fetchexcel() throws BiffException, IOException
	{
		String path = System.getProperty("user.dir")+"//demo.xls";
		
		Workbook wb = Workbook.getWorkbook(new File(path));
	Sheet sh = wb.getSheet(1);
	row = sh.getRows();
	col= sh.getColumns();
	String[][] excell = new String[row-1][col];
	for(int i=1;i<row;i++)
	{
		for(int j=0;j<col;j++)
		{
			Cell cell = sh.getCell(j,i);
			excell[i-1][j]= cell.getContents();
		} 
	}
	return excell;
	
	}
	
	}
