package Week6.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static Object[][] getExcelData(String filename) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/excel.xlsx");
		XSSFSheet sht = wbook.getSheetAt(0);
		int lastRownum = sht.getLastRowNum();
		System.out.println("Row total: " +lastRownum);
		int lastCellNum = sht.getRow(0).getLastCellNum();
		System.out.println("Column total " +lastCellNum);
		Object[][] val = new Object[lastRownum][lastCellNum];
		for(int j=1;j<=lastRownum;j++)
		{ 
			XSSFRow rw = sht.getRow(j);
		for(int i=0;i<lastCellNum;i++)
		{
			XSSFCell cll = rw.getCell(i);
			String stringCellValue = cll.getStringCellValue();
			val[j-1][i] = cll.getStringCellValue();
			//System.out.println(stringCellValue);
		}
		}
		return val;
	}
}
