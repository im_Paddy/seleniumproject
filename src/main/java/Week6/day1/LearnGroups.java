package Week6.day1;

import org.testng.annotations.Test;

public class LearnGroups {
	
	@Test(groups = "smoke")
	public void createlead()
	{
		System.out.println("Create lead functions twice");
		/*throw new RuntimeException();*/
	}
	@Test(groups = "sanity",dependsOnGroups = "smoke")
	public void deletelead()
	{
		System.out.println("Delete lead depends on create lead");
	}

	@Test(groups = "regression")
	public void editlead()
	{
		System.out.println("Edit lead");
	}

	@Test(groups = "smoke")
	public void TestNG()
	{
		System.out.println("Run");
	}
	@Test(groups = "sanity",dependsOnGroups = "smoke")
	public void TestNGrun()
	{
		System.out.println("Run method");
	}

	@Test(groups = "regression")
	public void TestNGrunning()
	{
		System.out.println("Running method");
	}
}
