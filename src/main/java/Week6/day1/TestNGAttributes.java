package Week6.day1;

import org.testng.annotations.Test;

public class TestNGAttributes {
	
/*@Test(invocationCount=2)
public void createlead()
{
	System.out.println("Create lead functions twice");
	throw new RuntimeException();
}
@Test(enabled=false)
public void deletelead()
{
	System.out.println("Delete lead depends on create lead");
}

@Test(dependsOnMethods= {"createlead"},alwaysRun=true)
public void editlead()
{
	System.out.println("Edit lead");
}*/

@Test
public void TestNG()
{
	System.out.println("Run");
}
@Test(priority = 2)
public void TestNGrun()
{
	System.out.println("Run method");
}

@Test(priority = -1)
public void TestNGrunning()
{
	System.out.println("Running method");
}
}
