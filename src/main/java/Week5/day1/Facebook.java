package Week5.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Facebook {

	public static void main(String[] args) throws InterruptedException {
	
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe" );
		 ChromeOptions obj = new ChromeOptions();
         obj.addArguments("--disable-notifications");
         ChromeDriver driver = new ChromeDriver(obj);
		    driver.manage().window().maximize();
            driver.get("https://www.facebook.com/");
            driver.findElementById("email").clear();
            driver.findElementById("email").sendKeys("");
            driver.findElementById("pass").clear();
            driver.findElementById("pass").sendKeys("");
            driver.findElementById("loginbutton").click();     
            driver.findElementByClassName("_1frb").sendKeys("TestLeaf");
            driver.findElementByXPath("//button[@aria-label='Search']").click();
            Thread.sleep(4000);
            WebElement wb = driver.findElementByXPath("(//button[@class='_42ft _4jy0 PageLikeButton _4jy3 _517h _51sy'])[1]");
            String str = wb.getText();
            if(str.equalsIgnoreCase("Like"))
            {
            	wb.click();
            }
            else
            {
            	System.out.println("Like Button is already clicked");
            }
            driver.findElementByLinkText("TestLeaf").click();
            Thread.sleep(4000);
            WebElement count = driver.findElementByXPath("//div[contains(text(),'people like this')]"); 
            System.out.println(count.getText());
	}

}
