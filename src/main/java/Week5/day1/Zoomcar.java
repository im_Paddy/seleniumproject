package Week5.day1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Zoomcar extends ProjectMethods 
{
	@BeforeTest
	public void name() {
		testCaseName = "TC_Zoomcar";
		testCaseDesc = "Zoomcar Testing";
		author = "paddy";
		category = "smoke";
	}
	@Test
	public void start() throws InterruptedException
	{
	 startApp("chrome", "https://www.zoomcar.com/");
	 Thread.sleep(4000);
	WebElement chn = locateElement("xpath","//div[text()='Chennai']");
	click(chn);
	WebElement journey = locateElement("xpath","//a[@title='Start your wonderful journey']");
	click(journey);
	WebElement porur = locateElement("xpath","//div[@class='component-popular-locations']/div[5]");
	click(porur);
	WebElement next = locateElement("xpath","//button[text()='Next']");
	click(next);
	Date date = new Date();
	DateFormat dt = new SimpleDateFormat("dd");
	String cd = dt.format(date);
	int tomo = Integer.parseInt(cd)+1;
    System.out.println(tomo);
    WebElement nextday = locateElement("xpath","//div[@class = 'day'][1]");
    click(nextday);
    WebElement next1 = locateElement("xpath","//button[text()='Next']");
	click(next1);
    WebElement done = locateElement("xpath","//button[text()='Done']");
	click(done);
	List<WebElement> carlist = driver.findElementsByXPath("//div[@class='price']");
	System.out.println(carlist.size());
	List<Integer> strlist = new ArrayList<Integer>();
	for (WebElement webElement : carlist) {
		String str = webElement.getText().replaceAll("\\D", "");
	    strlist.add(Integer.parseInt(str));
	}
	Integer maxvalue = Collections.max(strlist);
	System.out.println("Max Price- " +maxvalue);
}
}




