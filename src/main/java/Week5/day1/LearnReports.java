package Week5.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	public static void main(String[] args) {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.Html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_CreateLead","Create a new Lead ");
		//test.pass("Browser is launched",MediaEntityBuilder.createScreenCaptureFromPath("./../reports/img1.png")).build();
		test.pass("Browser is launched");
		test.pass("username is entered");
		test.pass("password is entered");
		test.pass("submit is clicked");
		test.assignCategory("Smoke");
		test.assignAuthor("Sethu");
		//test.pass("Browser is launched",MediaEntityBuilder.CreateScreenCaptureFrompath("./../reports/img1.png")).build();
		extent.flush();
	}

}
