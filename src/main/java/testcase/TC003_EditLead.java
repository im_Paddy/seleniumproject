package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods
{
	@BeforeTest
	public void name() {
		testCaseName = "Edit Lead";
		testCaseDesc = "Edit Lead Testing";
		author = "paddy";
		category = "smoke";
	}
	
@Test
public void edit()
{
	
	WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
	eleLeads.click();
	WebElement elefindleads = locateElement("xpath","//a[text()='Find Leads']");
	elefindleads.click();
	WebElement elefirstname = locateElement("xpath","(//input[@name='firstName'])[3]");
	type(elefirstname,"karthik");
	WebElement elecLeads = locateElement("xpath","//button[text()='Find Leads']");
	elecLeads.click();
	WebElement elefirstresult = locateElement("xpath","//a[text()='11106']");
	elefirstresult.click();
	String title = driver.getTitle();
	if(title.equals(title)) {
		System.out.println("Verifired text is correct" +title);
	}
	else {
		System.out.println("Verifired text is inncorrect");
	}
	WebElement eleedit = locateElement("xpath","(//a[@class='subMenuButton'])[3]");
	eleedit.click();
	
	WebElement eleupdate = locateElement("id","updateLeadForm_companyName");
	eleupdate.clear();
	type(eleupdate,"RUK Compay");
	
	WebElement eleclupdate = locateElement("xpath","(//input[@class='smallSubmit'])[1]");
	
	eleclupdate.click();
	driver.close();
	
}
}
