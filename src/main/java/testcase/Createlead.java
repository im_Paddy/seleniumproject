package testcase;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Week6.day1.LearnExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class Createlead extends ProjectMethods{
	
	@BeforeTest
	public void name() {
		testCaseName = "TC_Create Lead";
		testCaseDesc = "Create Lead Testing";
		author = "paddy";
		category = "smoke";
		excelFileName = "excel";
	}
@Test(dataProvider="fetchdata")
public  void crtlead(String cName, String fName, String lName) 
	{
	
	WebElement createLead = locateElement("linkText","Create Lead");	
	click(createLead);
	WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
	type(eleCompName, cName);
	WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
	type(eleFirstName, fName);
	WebElement eleLastName = locateElement("id","createLeadForm_lastName");
	type(eleLastName, lName);
	WebElement subMit = locateElement("class","smallSubmit");
	click(subMit);
	}




/*@DataProvider(name="positive")
public Object[][] fetchData(){
	Object[][] data = new Object[2][3];
	
	data[0][0]="Zogo";
	data[0][1]="sandy";
	data[0][2]="rock";
	
	data[1][0]="RBS";
	data[1][1]="Sai";
	data[1][2]="Ram";
	
	return data;*/


}

