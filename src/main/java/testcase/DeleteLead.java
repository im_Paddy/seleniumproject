package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLead extends ProjectMethods{
	@BeforeTest
	public void name() {
		testCaseName = "TC_DeleteLead";
		testCaseDesc = "DeleteLead Testing";
		author = "paddy";
		category = "smoke";

}
	@Test
	public void delete()
	{
		WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
		eleLeads.click();
		WebElement elefindleads = locateElement("xpath","//a[text()='Find Leads']");
		elefindleads.click();
		WebElement elephone = locateElement("xpath","//span[text()='Phone']");
		elephone.click();
		WebElement elephareacode = locateElement("xpath","//input[@name='phoneAreaCode']");
		type(elephareacode,"+91");
		WebElement elephonenumber = locateElement("xpath","//input[@name='phoneNumber']");
		type(elephonenumber,"1234567890");
		WebElement elefnleads = locateElement("xpath","//button[text()='Find Leads']");
		elefnleads.click();
	}
}